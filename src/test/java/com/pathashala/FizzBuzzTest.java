package com.pathashala;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FizzBuzzTest {
  @Test
  void expectFizzWhenNumber3() {
    FizzBuzz fizzBuzz = new FizzBuzz();

    assertEquals("Fizz", fizzBuzz.run(3));
  }

  @Nested
  class MultipleOfFive {
    @Test
    void expectBuzzWhenNumber5() {
      FizzBuzz fizzBuzz = new FizzBuzz();

      assertEquals("Buzz", fizzBuzz.run(5));
    }

    @Test
    void expectBuzzWhenNumber10() {
      FizzBuzz fizzBuzz = new FizzBuzz();

      assertEquals("Buzz", fizzBuzz.run(10));
    }

  }
}
