package com.pathashala;

class FizzBuzz {
  String run(int number) {
    if(isDivisibleByFive(number)) {
      return "Buzz";
    }
    return "Fizz";
  }

  private boolean isDivisibleByFive(int number) {
    return number % 5 == 0;
  }
}
